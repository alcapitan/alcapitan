# Bonjour et bienvenue sur mon profil 👋

## Qui suis-je ? 💁

J'ai 15 ans, je vis dans le Vaucluse (France), et je suis en classe de première au lycée.  
La programmation est une de mes activités préférés. Vous verrez ici tout mes projets présentables.
  
Ma citation est : "Si tu es contre le monde, alors tu es fait pour changer le monde." (proverbe indien)  
  
Suivez-moi pour voir toutes les avancées ! 😊  
  
_Allez voir mon portfolio pour en savoir plus et pour voir mes profils sur d'autres sites : [https://alcapitan.github.io/alcapitan](https://alcapitan.github.io/alcapitan)_

## Mes activités 👨‍💻

Je suis actuellement en train de rénover tout mes projets afin de les rendre viables.  
Vous voyez sur ce profil qu'une petite partie de mon travail déjà accompli.  
Je ne fais malheureusement que du front-end car je n'ai pas de serveur.

**Les languages que je pratique sont :**
* HTML 
* CSS
* JavaScript
* Python
  
**Et prochainement :**
* C++
* Java

**Les particularités de mes créations :** leur vitesse (battant largement les autres), leur accesibilité, et leur simplicité.  
  
**Allez voir les répertoires que j'ai épinglés et n'hésitez pas à m'envoyer vos retours via la rubrique "Discussions" ou "Issues" ! 👇**  

## Mon projet en parallèle : Al Capitan News 📰

En plus de la programmation, ma deuxième activité préféré est l'information/l'actualité.  
C'est pour cela que j'ai créé un compte actu sur Instagram.  
Je diffuse de l'actu générale mais je me concentre sur les infos peu diffusées. 
N'hésitez pas à y faire un tour et à vous abonner : [alcapitan.news](https://www.instagram.com/alcapitan.news)  

## Collaborations 🤝

Je suis volontaire pour des collaborations qui serait dans mes compétences.  
Envoyez moi un message (via les contacts dans mon portfolio), pour faire un échange de travail.  
  
N'hésitez pas à m'aider à faire évoluer mes projets via les "Discussions" "Issues" et le rôle "Collaborateur".  
